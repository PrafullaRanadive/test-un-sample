package main

import (
	"log"
	"net/http"
)

func main() {
	http.HandleFunc("/hello", handler)
	http.ListenAndServe(":8080", nil)
}

func handler(w http.ResponseWriter, r *http.Request) {

	params, ok := r.URL.Query()["name"]

	if !ok || len(params) < 1 {
		log.Println("Url Param 'name' is missing")
		return
	}

	// Query()["name"] will return an array of items,
	// we only want the single item.
	param := params[0]

	log.Println("Url Param 'name' is: " + string(param))
}
